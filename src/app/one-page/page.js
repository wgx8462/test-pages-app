'use client'

import {useState} from "react";
import {useRouter} from "next/navigation";

export default function OnePage() {
    const [myName, setMyName] = useState("");
    const router = useRouter();

    const handleName = e => {
        setMyName(e.target.value);
    }
    const handleSubmit = e => {
        e.preventDefault();
        // 페이지 이동은 스택을 사용해야한다.
        router.push(`/two-page?name=${myName}`)
    }

    return (
        <form onSubmit={handleSubmit} className="flex min-h-screen flex-col items-center p-24">
            <h1 className="font-semibold text-xl">당신의 이름은?</h1>
            <div>
                <input type="text" value={myName} onChange={handleName} placeholder="이름을 입력하세요"/>
            </div>
            <button type="submit" className="">다음 ⇒</button>
        </form>
    )
}