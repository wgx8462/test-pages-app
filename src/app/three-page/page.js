'use client'

import {useState} from "react"
import {useRouter, useSearchParams} from "next/navigation"

export default function OnePage() {
    const [gender, setGender] = useState('')
    const router = useRouter()
    const searchParams = useSearchParams()
    const name = searchParams.get("name")
    const age = searchParams.get("age")

    const handleGender = e => {
        setGender(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/four-page?name=${name}&age=${age}&gender=${gender}`)
    }

    return (
        <form onSubmit={handleSubmit} className="flex min-h-screen flex-col items-center p-24">
            <h1 className="font-semibold text-xl">당신의 성별은?</h1>
            <div>
            <select value={gender} onChange={handleGender}>
                <option value="">선택하세요</option>
                <option value="남자">남자</option>
                <option value="여자">여자</option>
            </select>
            </div>
            <button type="submit">다음 ⇒</button>
        </form>
    )
}