'use client'

import {useState} from "react";
import {useRouter, useSearchParams} from "next/navigation";

export default function OnePage() {
    const [age, setAge] = useState(0);
    const router = useRouter();
    const searchParams = useSearchParams();
    const name = searchParams.get("name");


    const handleAge = e => {
        setAge(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/three-page?name=${name}&age=${age}`)
    }

    return (
        <form onSubmit={handleSubmit} className="flex min-h-screen flex-col items-center p-24">
            <h1 className="font-semibold text-xl">당신의 나이는?</h1>
            <div>
                <input type="number" value={age} onChange={handleAge} placeholder="나이를 입력하세요"/>
            </div>
            <button type="submit">다음 ⇒</button>
        </form>
    )
}