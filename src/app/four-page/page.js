'use client'

import {useSearchParams} from "next/navigation"

export default function OnePage() {
    const searchParams = useSearchParams()
    const name = searchParams.get("name")
    const age = searchParams.get("age")
    const gender = searchParams.get("gender")


    return (
        <div className="flex min-h-screen flex-col items-center p-24">
            <p className="font-semibold text-xl">이름 : {name}</p>
            <p className="font-semibold text-xl">나이 : {age}</p>
            <p className="font-semibold text-xl">성별 : {gender}</p>
        </div>
    )
}